
    message =  "\nThis script determines the initial velocities required to break\n"
    message *= "a bond between two peridynamic cells.  The configuration considered\n"
    message *= "is a two-cell system with the cells moving away from each other\n"
    message *= "with equal and opposite prescribed initial velocities.\n"
    message *= "This code has been adapted from the script of the same name provided in the Peridigm repository."
    message *= "\nCopyright to Beno J Jacob under the GPL v3 license."
    println(message)

    # material properties
    k = 130.0e9            # bulk modulus
    μ = 60.0e9            # shear modulus
    ρ = 7800.0		  # density
    criticalStretch = 0.02

    ω = 1.0   # influence function

    # initial configuration
    cellVolume = 1.05
    mass = cellVolume*ρ
    initialDistance = 1.0
    weightedVolume = ω*initialDistance*initialDistance*cellVolume

    criticalExtension = initialDistance*criticalStretch
    criticalDistance = criticalExtension + initialDistance

    # the expression for the pairwise force in terms of the extension
    # t = C e

    C = (9.0*k - 15.0*μ)*ω*ω*initialDistance*initialDistance*cellVolume/(weightedVolume*weightedVolume) +
        15.0*μ*ω/weightedVolume

    # the work done in getting to criticalDistance is the integral of the total force (which is
    # two times the pairwise force) times one half delta extension (because the particles are
    # moving away from each other, the distance traveled for one cell is half the extension).
    # Both cells must be considered.

    # the cellVolumes are required to convert the force density per unit volume to a force

    work = 2.0*cellVolume*cellVolume*C*criticalExtension*criticalExtension/2.0

    # the critical initial velocity for bond breaking is determined by
    # setting the initial kinetic energy equal to the work required to
    # reach the critical stretch

    criticalVelocity = sqrt( work/mass )

    println("\nMaterial properties:")
    println("  bulk modulus =                  ", k/10^9, " GPa")
    println("  shear modulus =                 ", μ/10^9, " GPa")
    println("  density =                       ", ρ, " kg/m^3")
    println("  critical stretch =              ", criticalStretch)

    println("\nInitial configuration:")
    println("  cell volume =                   ", cellVolume)
    println("  initial distance =              ", initialDistance)

    println("\nMiscellaneous stuff:")
    println("  mass =                          ", mass)
    println("  weighted volume =               ",weightedVolume)
    println("  extension at critical stretch = ", criticalExtension)
    println("  distance at critical stretch =  ", criticalDistance)
    println("  work =                          ", work)
    println("\nCritical initial velocity =       ", criticalVelocity)
    println("")
