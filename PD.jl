
@everywhere using Distributed
@everywhere using NearestNeighbors
using Statistics
@everywhere using PolygonOps
using Gnuplot
using DataFrames
using CSV
@everywhere import GeometryBasics
@everywhere import Meshes
import GLMakie
import MeshViz
@everywhere using TOML
wp = CachingPool(workers())

@everywhere using ProgressMeter
#from ensight import Ensight
@everywhere include("materials.jl")

rank = myid()

### Peridynamic functions ###
@everywhere include("current_functions.jl")

#This line begins the main program.  This is not nescarry, but can be helpful
#if we want to load this file as a module from another Python script
#if __name__ == "__main__"

    #####################
    ### Main Program ####
    #####################
    # INPUTS
    @everywhere input_deck = TOML.parsefile("examples/disk_impact.toml")
    GRIDSIZE1 = 60
    GRIDSIZE2 = 45
    HORIZON = input_deck["Blocks"]["Disk Block"]["Horizon"]
    TIME_STEP = 1.e-6
    #TIME_STEP = nothing
    YOUNGS_MODULUS = 200.0e9
    POISSON_RATIO = 0.29
    RHO = 7800
    SAFTEY_FACTOR = 0.5
    MAX_ITER = 10000
    PLOT_DUMP_FREQ = 100
    VERBOSE = ~false
    length(ARGS) > 0 && ARGS[end] == "-v" && (VERBOSE = true)
    crackends = vcat(Meta.eval.(Meta.parse.(split(input_deck["Boundary Conditions"]["Precrack_nodeset"])))...)
    CRACKS = map((x,y)-> (x,y), crackends[1:end-1], crackends[2:end])
    CRACKS1 = CRACKS[1:82]
    CRACKS2 = [CRACKS[83]]
    CRACKS3 = CRACKS[84:end]
    #CRACKS = [[GRIDSIZE/2., -1., GRIDSIZE/2., GRIDSIZE/10.],[GRIDSIZE/2.,
    #    9*GRIDSIZE/10. , GRIDSIZE/2., GRIDSIZE+1.],[5.,10,20.,40.]]
    
    #Create x,y,z of node positions
    # Comment the following line if the discretisation is through a method
    # unassociated with gmshnodeparser
    @everywhere nodes = gmshnodeparser(input_deck)
    global_number_of_nodes = length(nodes)

    MAX_NEIGHBORS_RETURNED = 300
    BC1_POLYGON = [(maximum(x_coordinate.(nodes))+0.0001, minimum(y_coordinate.(nodes))-0.0001),
                   (maximum(x_coordinate.(nodes))-HORIZON, minimum(y_coordinate.(nodes))-0.0001),
                   (maximum(x_coordinate.(nodes))-HORIZON, maximum(y_coordinate.(nodes))+0.0001),
                   (maximum(x_coordinate.(nodes))+0.0001, maximum(y_coordinate.(nodes))+0.0001),
                   (maximum(x_coordinate.(nodes))+0.0001, minimum(y_coordinate.(nodes))-0.0001)]
    BC2_POLYGON = [(minimum(x_coordinate.(nodes))-0.0001, minimum(y_coordinate.(nodes))-0.0001),
                   (minimum(x_coordinate.(nodes))+HORIZON, minimum(y_coordinate.(nodes))-0.0001),
                   (minimum(x_coordinate.(nodes))+HORIZON, maximum(y_coordinate.(nodes))+0.0001),
                   (minimum(x_coordinate.(nodes))-0.0001, maximum(y_coordinate.(nodes))+0.0001),
                   (minimum(x_coordinate.(nodes))-0.0001, minimum(y_coordinate.(nodes))-0.0001)]
    BC1_VALUE = -50.
    BC2_VALUE = 50.
    VIZ_PATH=input_deck["Outputs"]["Output Folder"]

    #Print version statement
    rank == 1 && println("PD.jl version 0.0.1\n")

    #Set up the grid


        
    node_matrix_for_kdtree = [x_coordinate.(nodes) y_coordinate.(nodes) z_coordinate.(nodes)]'
    #Create a kdtree to do nearest neighbor search
    tree = NearestNeighbors.KDTree(node_matrix_for_kdtree)

    #Get all families
    families = inrange(tree, node_matrix_for_kdtree, HORIZON)
	[setdiff!(families[i],i) for i in eachindex(families) if i ∈ families[i]]

    #insert cracks
    if size(CRACKS) != 0
        println("Inserting precracks...\n")
    end
    # This crack setup is problem specific
    for crack in CRACKS1
        #Loop over and insert precracks.  The 1e-10 term is there to reduce the
        #chance of the crack directly intersecting any node and should not affect
        #the results at all for grid spacings on the order of 1.
        insert_crack(nodes[crack[1]] .- 1e-5, nodes[crack[2]] .- 1e-5, tree, HORIZON, nodes, families)
    end
    for crack in CRACKS2
        #Loop over and insert precracks.  The 1e-10 term is there to reduce the
        #chance of the crack directly intersecting any node and should not affect
        #the results at all for grid spacings on the order of 1.
        insert_crack(nodes[crack[1]] .- 1e-5, nodes[crack[2]] .+ 1e-5, tree, HORIZON, nodes, families)
    end
    for crack in CRACKS3
        #Loop over and insert precracks.  The 1e-10 term is there to reduce the
        #chance of the crack directly intersecting any node and should not affect
        #the results at all for grid spacings on the order of 1.
        insert_crack(nodes[crack[1]] .+ 1e-5, nodes[crack[2]] .+ 1e-5, tree, HORIZON, nodes, families)
    end

    position_x = x_coordinate.(nodes)
    position_y = y_coordinate.(nodes)
       #Compute reference position state of all nodes
#       my_reference_position_state_x = map(node_index -> position_x[families[node_index]] .- position_x[node_index], (eachindex(families)))
#       my_reference_position_state_y = map(node_index -> position_y[families[node_index]] .- position_y[node_index], (eachindex(families)))
       my_reference_position_state = [compute_position_state(nodes, node_index, node_family) for (node_index, node_family) in enumerate(families)]

    #Compute reference magnitude state of all nodes
#    my_reference_magnitude_state = map(node_index -> sqrt.(my_reference_position_state_x[node_index] .^2 .+ my_reference_position_state_y[node_index] .^2), (eachindex(families)))
my_reference_magnitude_state = [magnitude.(arr) for arr in my_reference_position_state]

    #Initialize influence state
    my_influence_state = influence_function(my_reference_magnitude_state,HORIZON)
    #Create a reference copy, used for normalizing damage to a reference state
    my_reference_influence_state = copy(my_influence_state)

    #Initialize the dummy volumes
    my_volumes = ones(size(position_x))

    #Compute weighted volume
    weighted_volume = map(node_index -> sum(my_influence_state[node_index] .* my_reference_magnitude_state[node_index] .^2 .* my_volumes[families[node_index]]), (eachindex(my_reference_magnitude_state)))

    #Create distributed vectors (owned only)

    #Temporary arrays
    my_displacement = zeros(GeometryBasics.Point3, size(nodes))
    my_velocity = zeros(GeometryBasics.Point3, size(my_displacement))
    my_accelaration = zeros(GeometryBasics.Point3, size(my_displacement))
    my_accelaration_old = zeros(GeometryBasics.Point3, size(my_displacement))
    @everywhere my_force = zeros(GeometryBasics.Point3, size(nodes))
    # @everywhere my_force_state = collect(zeros(GeometryBasics.Point3, length(arr)) for arr in families)
    my_damage = zeros(size(nodes))

    #Initialize output files
    vector_variables = ["displacement"]
    scalar_variables = ["damage"]
    #Instantiate output file object
    #outfile = Ensight("output", vector_variables, scalar_variables, comm,
    #        viz_path=VIZ_PATH)
    #Print the temporary output arrays
    if rank == 1
        println("Output variables requested:\n")
        for item in vector_variables
            println("    ", item)
        end
        for item in scalar_variables
            println("    " , item)
        end
        println(" ")
    end

    #Find local nodes where boundary conditions should be applied
    bc1_local_node_set = boundary_condition_set(BC1_POLYGON,nodes)
    bc2_local_node_set = boundary_condition_set(BC2_POLYGON,nodes)

    #Calculate a stable time step or use the user defined
    if TIME_STEP === nothing
        time_step = SAFTEY_FACTOR*compute_stable_time_step(families,
                my_reference_magnitude_state, my_volumes, YOUNGS_MODULUS, RHO, HORIZON)
    else
        time_step = TIME_STEP
    end

    #Begin the main explicit time stepping loop, the VERBOSE variable sets either
    #a progress bar or verbose output here.
    #Time stepping loop
    @showprogress 1 "Computing ..." for iteration in 1:MAX_ITER

        #Set current time
        time = iteration * time_step

        #Print an information line
        VERBOSE && println("iter = ", iteration, " , time step = ",
                    time_step, " , sim time = ", time)

        #Enforce boundary conditions
        for node_index in bc1_local_node_set
            my_displacement[node_index] = (time * BC1_VALUE, 0.0, 0.0)
            my_velocity[node_index] = (BC1_VALUE, 0.0, 0.0)
        end
        #
        for node_index in bc2_local_node_set
            my_displacement[node_index] = (time * BC2_VALUE, 0.0, 0.0)
            my_velocity[node_index] = (BC2_VALUE, 0.0, 0.0)
        end

        #Clear the internal force vectors
        my_force[:] .= zeros(GeometryBasics.Point3, length(my_force))

        #Communicate the displacements (previous or boundary condition imposed)
        #Compute the internal force
        # my_force[:] = my_force .+ map(compute_internal_force,
        #                             Iterators.repeated(my_force, length(nodes)),
        #                             Iterators.repeated(nodes, length(nodes)),
        #                             Iterators.repeated(my_displacement, length(nodes)),
        #                             eachindex(families),
        #                             families,
        #                             my_reference_magnitude_state,
        #                             Iterators.repeated(my_volumes, length(nodes)),
        #                             YOUNGS_MODULUS,
        #                             POISSON_RATIO,
        #                             my_influence_state)

#        @time my_force_state[:] .= compute_force_state.(
#                             Iterators.repeated(nodes, length(nodes)),
#                             Iterators.repeated(my_displacement, length(nodes)),
#                             eachindex(families),
#                             families,
#                             my_influence_state,
#                             my_reference_magnitude_state,
#                             Iterators.repeated(weighted_volume, length(nodes)),
#                             YOUNGS_MODULUS,
#                             POISSON_RATIO
#                             )
@time my_force_state = pmap(wp, enumerate(families)) do (node_index, node_family)
         compute_force_state(
                                          nodes,
                             my_displacement,
                             node_index,
                             node_family,
                             my_influence_state[node_index],
                             my_reference_magnitude_state[node_index],
                             weighted_volume,
                             YOUNGS_MODULUS,
                             POISSON_RATIO
                             )
 end

        # @time my_force[:] = my_force .+ integrate_nodal_forces.(
        #                               Iterators.repeated(my_force_state, length(nodes)),
        #                               eachindex(nodes),
        #                               Iterators.repeated(families, length(nodes)),
        #                               Iterators.repeated(my_volumes, length(nodes)),
        #                              )
        @time my_force_local = pmap(wp, eachindex(nodes)) do node_index
                 integrate_nodal_forces(
                                      my_force_state,
                                      node_index,
                                      families,
                                      my_volumes,
                                     )
        end
    for node_index in eachindex(families)
        my_force[node_index] = my_force[node_index] .+ my_force_local[node_index]
    end
        #Compute the nodal acceleration
        my_accelaration_old[:] .= my_accelaration
        my_accelaration[:] .= my_force  ./ RHO

        #Compute the nodal velocity
        my_velocity[:] = my_velocity .+ 0.5 .* (my_accelaration_old .+ my_accelaration) .* time_step

        #Compute the new displacements
        my_displacement[:] = my_displacement .+ my_velocity .* time_step .+ (0.5 .* my_accelaration .* time_step .* time_step)

	    my_damage .= 1.0 .- mean.(my_influence_state[node_index] ./
				     my_reference_influence_state[node_index] for node_index in eachindex(families))

        #Dump plots
        if iteration % PLOT_DUMP_FREQ  == 0 || iteration == (MAX_ITER-1)
            VERBOSE && println("Writing plot file...")

            #Compute the damage

	    file = string(VIZ_PATH,"/output",iteration,".csv")

	    df = DataFrame()
	    df.:Nodes = eachindex(families)
	    df.:x_coordinate = position_x
	    df.:y_coordinate = position_y
	    df.:Displacment_x = x_coordinate.(my_displacement)
	    df.:Displacment_y = y_coordinate.(my_displacement)
        df.:Force_x = x_coordinate.(my_force)
        df.:Force_y = y_coordinate.(my_force)
	    df.:Damage = my_damage

	    CSV.write(file,df)
	end
        #Update the progress bar
        #VERBOSE || progress.update(iteration + 1)
    end

    #Finalize plotfiles
    GLMakie.lines([nodes[i] for crack in CRACKS for i in crack])
    #outfile.finalize()
