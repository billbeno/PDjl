[_PD jl_](_PD.jl_) peridynamics example code
===============================

_PD.jl_ is a 2D explicit-time integration code that serves as an example code
of how one might write a peridynamics code in parallel in julia primarily with `pmap`. A sequential code can be formed by replacing the `pmap` constructs  with the sequential `map`. The
code is heavily commented to provide as much insight as possible.

This code was initially adapted from the excellently commented PDpy code from Dr. John T. Foster.
However, there is much difference in the implementation as PDpy depends on PyTrilinos for parallel
execution while PDjl is purely julian. I have tried to keep up with comments to make this code a
good learning resource as PDpy has been for me.

To clone the repo:

````
git clone https://gitlab.com/billbeno/PDjl.git
````

#### Julia packages available via `Pkg.add()` ####
Distributed, NearestNeighbors, PolygonOps, ProgressMeter

To run the code:

````
julia -p auto PD.jl
````

where `auto` can be replaced with any arbitrary number of processsors.

The results can not yet be viewed in parallel with [Paraview](http://www.paraview.org/), but I hope to implement it ASAP.
