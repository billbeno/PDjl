using BenchmarkTools
### Peridynamic functions ###

""" 
    Position Vector State **X** ∈ *V*
    X(ξ) = ξ, ξ = x'-x
"""
compute_position_state(current_position, node_index, node_family) = current_position[node_family] .- current_position[node_index]

#Compute deformation unit state
position_unit_state(position_state, position_magnitude_state = magnitude(position_state)) = position_state ./ position_magnitude_state

#Apply a critical stretch damage model
extension_mask(extension_state, critical_stretch) = extension_state .> critical_stretch

function compute_force_state(current_position,
                        displacement,
                        node_index::Int64,
                        node_family::Vector{Int64},
                        influence_state,
                        reference_magnitude_state,
                        weighted_volume,
                        youngs_modulus,
                        poisson_ratio,
                        critical_stretch = 0.005
                        )  

        #Compute the deformed positions of the nodes
        #deformed_position = current_position + displacement
        #de

        #Compute deformation state
        deformation_state = compute_position_state(current_position+displacement, node_index, node_family)

        #Compute deformation magnitude state
        deformation_magnitude_state = magnitude.(deformation_state)

        #Compute deformation unit state
        deformation_unit_state = position_unit_state(deformation_state, deformation_magnitude_state)

        #Compute scalar extension state
        extension_state = deformation_magnitude_state .- reference_magnitude_state

        #Apply a critical stretch damage model
        my_extension_mask = extension_mask(extension_state, critical_stretch)
        influence_state[my_extension_mask] .= 0

        #Compute dilatation
        #dilatation = sum(3 .* weighted_volume[node_index] .* influence_state_node[node_index] .*
        #        reference_magnitude_state .* extension_state .* volumes[node_family])

        #Compute scalar force state
        #scalar_force_state = materials.elastic_material(youngs_modulus,
                #poisson_ratio, dilatation, extension_state, reference_magnitude_state,
                #weighted_volume, influence_state_node)
        scalar_force_state = bond_based_elastic_material(extension_state,
	    					     weighted_volume[node_family], youngs_modulus, poisson_ratio, influence_state)

        #Compute the force state
        return scalar_force_state .* deformation_unit_state
 
end

function integrate_nodal_forces(force_state, node_index, families, volumes)
    #Integrate nodal forces
    #Sum the force contribution from j nodes to i node
    # @time forces = map(enumerate(families)) do (node_index,node_family)
    j_2_i = sum(force_state[node_index] .* volumes[families[node_index]])

    location_i_node_in_family = [findfirst(x->x==node_index,families[cnode]) for cnode in families[node_index]]
    i_2_j = sum(force_state[cnode][location_i_node_in_family[idx]] .* volumes[node_index] for (idx,cnode) in enumerate(families[node_index]) if location_i_node_in_family[idx] != nothing)

    return j_2_i - i_2_j
end

# Internal force calculation
function compute_internal_force(
        forces,
        current_position,
        displacement,
        node_index,
        node_family,
        reference_magnitude_state,
        volumes,
        youngs_modulus,
        poisson_ratio,
        influence_state,
        critical_stretch = 0.005
    )
    """ Computes the peridynamic internal force due to deformations."""

    force_state = compute_force_state(current_position,
                        displacement,
                        node_index,
                        node_family,
                        influence_state,
                        reference_magnitude_state,
                        youngs_modulus,
                        poisson_ratio,
                        critical_stretch
                        )

    function integrate_nodal_forces(force_state, node_index, families, volumes)
    #Integrate nodal forces
    #Sum the force contribution from j nodes to i node
    # @time forces = map(enumerate(families)) do (node_index,node_family)
    j_2_i = sum(force_state[node_index] .* volumes[families[node_index]])

    location_i_node_in_family = [findfirst(x->x==node_index,families[cnode]) for cnode in families[node_index]]
    i_2_j = sum(force_state[cnode][location_i_node_in_family[idx]] * volumes[node_index] for (idx,cnode) in enumerate(families[node_index]) if location_i_node_in_family[idx] != nothing)

    return j_2_i - i_2_j
	end
    #Subtract the force contribution from i nodes from j, the bincount()
    #operation is a trick to keep it fast in Numpy.  See:
    #<http://stackoverflow.com/questions/9790436/numpy-accumulating-one-array-
    #in-another-using-index-array> for details
    #j==1 ? force_x .+= force : force_y .+= force
    end

#Compute stable time step function
function compute_stable_time_step(families, reference_magnitude_state, volumes,
        bulk_modulus,rho,horizon)

    spring_constant = 18.0 * bulk_modulus / pi / horizon^4.0

    crit_time_step_denom = ([spring_constant .* volumes[families[i]] ./
				  reference_magnitude_state[i] for i in eachindex(families)])

    critical_time_steps = [sqrt(2.0 .* rho) ./ crit_time_step_denom[i] for i in eachindex(families)]

    nodal_min_time_step = [ minimum(setdiff(item,0)) for item in critical_time_steps ]

    return minimum(nodal_min_time_step)
end

#Helper function that tests to see if two lines intersect
# Can include a better algorithm hopefully with the Meshes package.
function test_line_segment_intersect(line1,line2)
    """Tests to see if two lines segments intersect.  The lines are defined as:

       line1 = [ p0_x, p0_y, p1_x, p1_y ]
       line2 = [ p2_x, p2_y, p3_x, p3_y ]

    """
    #See http://stackoverflow.com/questions/563198/how-do-you-detect-where-
    #two-line-segments-intersect for algorithm details

    #Read in individual point x,y positions from arguments
    p0_x, p0_y, p1_x, p1_y = line1
    p2_x, p2_y, p3_x, p3_y = line2

    s1_x = p1_x - p0_x
    s1_y = p1_y - p0_y
    s2_x = p3_x - p2_x
    s2_y = p3_y - p2_y

    denom = (-s2_x * s1_y + s1_x * s2_y)

    num_s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y))
    num_t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x))

    #Detect if lines are parallel or coincident
    if -1e-10 < denom < 1e-10
	#Test if lines are conincident ? #Lines are conincident : #Parallel, but not conincident
        return abs(num_s) - abs(num_t) < 1e-5
    else
        s =  num_s  / denom
        t =  num_t / denom

        return 0.0 <= s <= 1.0 && 0.0 <= t <= 1.0
        #Lines intersect (or meet at endpoints) if true
    end
end

#Inserts a crack by removing neighbors from family lists
function insert_crack(crackend1, crackend2, tree, horizon, nodes, families)
    """
       Inserts crack by setting influence_state to zero for bonds that cross
       crack path.
    """

    #Read in crack endpoints
    x_ep1, y_ep1 = crackend1
    x_ep2, y_ep2 = crackend2
    crack = [x_ep1, y_ep1, x_ep2, y_ep2]

    #Calculate crack length
    crack_length_x = x_ep2 - x_ep1
    crack_length_y = y_ep2 - y_ep1
    crack_length = sqrt(crack_length_x ^ 2.0 + crack_length_y ^ 2.0)

    #Number of discrete points along crack length
    number_points_along_crack::Int = ceil(crack_length / horizon * 4.0)

    #Find slope of crack line
    slope_denom = crack_length_x
    if -1e-10 < slope_denom < 1e-10
        #Crack line is vertical, discrete points along crack path
	x_points = x_ep1 .* ones(number_points_along_crack)
	y_points = range(y_ep1,y_ep2; length=number_points_along_crack)
    else
        slope = (y_ep2 - y_ep1) / slope_denom
        #line_eqn = x-> slope * (x - x_ep1) + y_ep1
        #Find the discrete points along crack path
	x_points = range(x_ep1,x_ep2; length=number_points_along_crack)
	y_points = range(y_ep1,y_ep2; length=number_points_along_crack)
    end

    #Create a tuple required by the scipy nearest neighbor search
    points_along_crack = [x_points y_points zeros(length(x_points))]

    #Find all nodes that could possibly have bonds that cross crack path
    #_, nodes_near_crack = tree.query(points_along_crack,
    #        k=MAX_NEIGHBORS_RETURNED, eps=0.0, p=2,
    #        distance_upper_bound=2.0*horizon)
    nodes_near_crack = inrange(tree, points_along_crack', 2*horizon)

    #The search above will produce duplicate neighbor nodes, make them into a
    #unique 1-dimensional list
    nodes_near_crack_flat = unique!(vcat(nodes_near_crack...))

    #Remove the dummy entries
    #nodes_near_crack_flat = nodes_near_crack_flat[nodes_near_crack_flat !=
    #        tree.n]

    #Loop over nodes near the crack to see if any bonds in the nodes family
    #cross the crack path
    for node_index in nodes_near_crack_flat
        #Loop over node family
        node_family = families[node_index]
	    for (bond_index,end_point_index) in enumerate(node_family)
            #Define the bond line segment as the line between the node and its
            #endpoint.
            bond_line_segment = [ x_coordinate(nodes[node_index]), y_coordinate(nodes[node_index]),
                                 x_coordinate(nodes[end_point_index]), y_coordinate(nodes[end_point_index]) ]
            #Test for intersection
            test_line_segment_intersect(crack,bond_line_segment) &&
            #If we got to here that means we need to ``break'' the bond
	        deleteat!(families[node_index], bond_index)
        end
    end

    return
end


function boundary_condition_set(vertices,nodes)
    """Finds nodes enclosed by the polygon described with vertices"""

    #Create a polygon object with a list of vertices, vertices must be tuples/arrays. Important that the first and last vertices are the same.
    #Returns an array with value True if point is inside polygon, False if not
    bool_arr = [PolygonOps.inpolygon(nodes[p], vertices; in=true,on=false,out=false) for p = eachindex(nodes)]
    #Returns local node indices that are inside the polygon
    return findall(bool_arr)
end

function influence_function(reference_magnitude_state,horizon)
    """Returns an influence state that has the form 1 - ξ/δ"""
    return map( eachindex(reference_magnitude_state)) do node_index
	    1 .- reference_magnitude_state[node_index] ./ horizon
    end
end

""" 
The 'gmshnodeparser' function parses the '<mesh.pos>' file exported from GMSH after post processing to extract the node nodes. The processing involved are as follows:
    1. Create geometry in gmsh.
    2. Create the mesh in gmsh.
    3. Create an empty view with Tools>Plugins>NewView in gmsh.
    4. Extract the x,y,z data with Tools>Plugins>MathEval in gmsh.
    5. Then export the view. Set the format option to 'Mesh-based' when exporting as '.pos'.

The function returns an array with element type 'GeometryBasics.Point3'.
The nodenumbering is the same as that give in gmsh.
"""
function gmshnodeparser(input_file)
    x = readlines(input_file["Discretization"]["Input Mesh File"])
    nodestart = findfirst(x .== "\$Nodes") + 2
    nodeend = findfirst(x .== "\$EndNodes") - 1
    collect(float.(GeometryBasics.Point3(Meta.parse.(split(str))...)) for str in x[nodestart:nodeend] if length(split(str)) ==3)
end

x_coordinate(a) = a[1]
y_coordinate(a) = a[2]
z_coordinate(a) = a[3]
magnitude(arr) = sqrt(sum(arr.^2))

"""
precrack as a function introduces lines across which family members lose interaction.
The idea is to identify the end points of a line.
    - Find each point lying on the pre-crack or approximately close to.
    - Sort the points according to one of the coordinates, this help to determine
      line segments without overlap.
    - This approach helps with relocating the cracks with time as crack face may also
      move as the simulation continues.
    - Further, in the case of contact problems, this can assist with determining contact
      and reestablishing family members due to the contact.
"""
function precrack(endpoint1, endpoint2)
    # Some calculation
end


