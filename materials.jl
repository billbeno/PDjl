
# Simple constitutive model
function bond_based_elastic_material(extension_state, weighted_volume, youngs_modulus,
        poisson_ratio, influence_state)
    """Computes the scalar force state.  This is the state-based version of a
       bond based material."""
    bulk_modulus = youngs_modulus / (3.0 * (1.0 - 2.0 * poisson_ratio))

    #Return the force
    return (9.0 .* bulk_modulus ./ weighted_volume .*
            extension_state .* influence_state)
end

# Linear peridynamic solid model
function elastic_material(youngs_modulus, poisson_ratio, dilatation, extension_state,
        reference_magnitude_state, weighted_volume, influence_state)

    #Convert the elastic constants
    bulk_modulus = youngs_modulus / (3.0 * (1.0 - 2.0 * poisson_ratio))
    shear_modulus = youngs_modulus / (2.0 * (1.0 + poisson_ratio))

    #Compute the pressure
    pressure = -bulk_modulus * dilatation

    #Compute the deviatoric extension state
    deviatoric_extension_state = (extension_state - dilatation[:,None] * reference_magnitude_state / 3.0)

    #Compute the peridynamic shear constant
    alpha = 15.0 * shear_modulus / weighted_volume

    #Compute the isotropic and deviatoric components of the force scalar state
    isotropic_force_state = (-3.0 * pressure[:,None] / weighted_volume[:,None] * influence_state * reference_magnitude_state)
    deviatoric_force_state = alpha[:,None] * influence_state * deviatoric_extension_state

    #Return the force scalar-state
    return isotropic_force_state + deviatoric_force_state
end
